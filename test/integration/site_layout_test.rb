require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "layout links before logging in" do
    get root_path
    assert_select "title", full_title("")
    assert_template 'static_pages/home'
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", users_path, count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
    assert_select "a[href=?]", edit_user_path(@user), count: 0
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", signup_path
    base_screen_layout
    get signup_path
    assert_select "title", full_title("Sign up")
  end

  test "Layout links after logging in" do
    log_in_as(@user)
    assert is_logged_in?
    assert_redirected_to @user
    follow_redirect!
    assert_select "title", full_title(@user.name)
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", user_path(@user)
    assert_select "a[href=?]", edit_user_path(@user)
    assert_select "a[href=?]", logout_path
    base_screen_layout
    assert_select "a[href=?]", signup_path, count: 0
  end

  test "Layout links after logging in and after logging out" do
    log_in_as(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    follow_redirect!
#    puts
#    puts full_title("")
#    puts
#    puts
#    puts @response.body
    assert_select 'title', full_title("")
    assert_template 'static_pages/home'
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", users_path, count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
    assert_select "a[href=?]", edit_user_path(@user), count: 0
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", signup_path
    base_screen_layout
    get signup_path
    assert_select "title", full_title("Sign up")
  end

  test "Check Counts of Posters User is Following" do
    log_in_as(@user)
    get root_path
#    corpse = response.body)
#    puts corpse
#
#    assert_select "a[href=?]", following_user_path(@user) do
#      assert_select "strong", text: "#{@user.following.count}"
    assert_select "a[href=?]", following_user_path(@user) do |elements|
      assert_equal elements.text.force_encoding("utf-8").split, ["#{@user.following.count}", "following".to_s.force_encoding("utf-8")]
    end
    assert_select "a[href=?]", followers_user_path(@user) do |elements|
      assert_equal elements.text.force_encoding("utf-8").split, ["#{@user.followers.count}", "followers".to_s.force_encoding("utf-8")]
    end
#      assert_match elements.text, (/.*following.*/).to_s
# .force_encoding("utf-8")
#    end
#      assert_match  "#{@user.following.count}"
#      assert_select text: "following"
#    end
  end
end
