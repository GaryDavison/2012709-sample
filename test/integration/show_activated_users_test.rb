require 'test_helper'

class ShowActivatedUsersTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
    @unactivated_user = users(:lana)
  end

  test "index including activation" do
    log_in_as(@user)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination', count: 2
    assert_select 'a[href=?]', user_path(@other_user), text: @other_user.name, count: 1
    assert_select 'a[href=?]', user_path(@unactivated_user), text: @unactivated_user.name, count: 0
    get user_path(@other_user)
    assert_template 'users/show'
    assert_select 'h1', text: @other_user.name, count: 1
    get user_path(@unactivated_user)
    assert_redirected_to root_url
  end


end
